
class Component {
    constructor(domNode, options) {
        this.domNode = domNode;
        this.data = options;
        this.render();
        this.init()
    }
    render() {
    }

    init() {
    }
}

export default Component;