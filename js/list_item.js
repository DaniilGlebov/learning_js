'use strict';

export default class ListItem{

 constructor(name){
     this.name = name;
     this.status = "active";
 }

 createListItem(){
     this.completeCheckbox = document.createElement('input');
     this.completeCheckbox.className = "toggle";
     this.completeCheckbox.type = "checkbox";
     this.removeButton = document.createElement("button");
     this.removeButton.className = "destroy";
     this.labelText = document.createElement('label');
     this.labelText.textContent = this.name;
     this.listItem = document.createElement("li");
     this.editInput = document.createElement("input");
     this.editInput.className = "edit";
     this.editInput.addEventListener("focusout", this.editTextOpen.bind(this));
     this.listItem.appendChild(this.completeCheckbox);
     this.listItem.appendChild(this.labelText);
     this.listItem.appendChild(this.removeButton);
 }

 changeStatus(){
     if (this.status === "active") {
         this.status = "completed";
         this.listItem.className = "completed";
     } else {
         this.status = "active";
         this.listItem.className = "";
     }
 }

 editTextOpen(){
     if(!this.listItem.querySelector(".edit")) {
         this.listItem.appendChild(this.editInput);
         this.editInput.focus();
         this.editInput.value = this.labelText.innerText;
         this.labelText.style.setProperty("display", "none");
         this.removeButton.style.setProperty("display", "none");
     } else if (this.editInput.value.replace(/ /gi, "") !== "") {
             this.labelText.innerText = this.editInput.value;
             this.labelText.style = "";
             this.removeButton.style = "";
             this.name = this.editInput.value;
             this.listItem.removeChild(this.editInput);
         }
     }


}