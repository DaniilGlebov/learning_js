
export default class EventsForList {
    constructor(list, footer){
        this.footer = footer;
        this.list = list;
    }

    addListenersToList() {
        this.list.root.addEventListener("removeElement", this.removeItemFromUl.bind(this));
        this.list.root.addEventListener("updateElement", this.editLi.bind(this));
        this.list.root.addEventListener("checkElement", this.checkItems.bind(this));
    }

    removeItemFromUl(liItem) {
        arrItems.find(function (obj) {
            if (obj.listItem === liItem.detail) {
                arrItems.splice(arrItems.indexOf(liItem.detail), 1);
            }
        });
        this.footer.changeNumOfItemsText();
        checkItemsLeft();
    }

    checkItems(liItem) {
        let item = this.list.data.find(function (obj) {
            if (obj.listItem === liItem.detail) {
                return obj;
            }
        });
        item.changeStatus();
        if (item.status !== document.location.hash.replace("#/","") && document.location.hash.replace("#/","") !== ""){
            this.list.removeItem(liItem.detail)
        }
        this.footer.changeNumOfItemsText();
        this.footer.visibleClearAllButton();
        checkAllCheckbox.checked = checkAllCheckbox.checked ? !checkAllCheckbox.checked : checkAllCheckbox.checked;
    }

    editLi(liItem) {
        arrItems.find(function (obj) {
            if (obj.listItem === liItem.detail) {
                obj.editTextOpen()
            }
        });
    }
}