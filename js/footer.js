
export default class Footer{

    constructor(){
        this.itemsNum = document.querySelector(".todo-items-left");
        this.clearCompletedButton = document.querySelector(".clear-completed");
        this.allFilter = document.querySelector('[href="#/"]');
        this.activeFilter = document.querySelector('[href="#/active"]');
        this.completedFilter = document.querySelector('[href="#/completed"]');
        this.clearCompletedButton.addEventListener("click", this.removeAllCompleted.bind(this));
    }

    removeAllCompleted() {
        todoListDiv.removeChild(listUl.root);
        let index = 0;
        while (index < arrItems.length){
            if(arrItems[index].status === "completed"){
                arrItems.splice(index, 1)
            } else {
                ++index;
            }
        }
        if (document.location.hash.replace("#/","") !== "completed") {
            listUl.data.arrItems = arrItems;
            listUl.render();
            listUl.init();
            actions.list = listUl;
            actions.addListenersToList();
        }
        this.clearCompletedButton.style.setProperty("display", "none");
        checkItemsLeft();
    }

    visibleClearAllButton() {
        let styleNew = "none";
        arrItems.forEach(function (obj) {
            if(obj.status === "completed"){
                styleNew = "";
            }
        });
        this.clearCompletedButton.style.setProperty("display", styleNew);
    }

    changeNumOfItemsText(){
        let num = 0;
        arrItems.forEach(function (obj) { if (obj.status === "active"){++num}});
        this.itemsNum.innerHTML = num ===1 ? num + " item left" : num + " items left";
    }

    filtering(){
        if(document.querySelector(".todo-list")) {
            todoListDiv.removeChild(listUl.root);
        }
        let filterName = document.location.hash.replace("#/","");
        filterName = filterName === "" ? "all" : filterName;
        document.querySelector(".selected").className = "";
        this[`${filterName}Filter`].className = "selected";
        let filteredArr = [];
        if(filterName !== "all") {
            arrItems.forEach(function (obj) {
                if (obj.status === filterName) {
                    filteredArr.push(obj)
                }
            });
        } else {
            filteredArr = arrItems;
        }
        listUl.data = filteredArr;
        listUl.render();
        listUl.init();
        actions.list = listUl;
        actions.addListenersToList();
    }
}