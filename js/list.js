import Component from "./component.js"

export default class List extends Component{

    addItem(item){
        this.root.appendChild(item.listItem);
    }

    removeItem(item) {
        this.root.removeChild(item);
    }

    updateItem(item) {
        this.root.dispatchEvent(new CustomEvent('updateElement',{
            bubbles: true,
            detail: item
        }));
    }

    checkUncheckItem(item) {
        this.root.dispatchEvent(new CustomEvent('checkElement',{
            bubbles: true,
            detail: item
        }));
    }

    init() {
        this.root.addEventListener('click', this.onRootClick.bind(this));
        this.root.addEventListener('dblclick', this.onRootDblClick.bind(this));
        this.root.addEventListener("keydown", this.onRootPressEnter.bind(this));
    }

    render() {
        this.root = document.createElement('ul');
        this.root.className = "todo-list";
        this.renderItems();
        this.domNode.appendChild(this.root)
    }

    renderItems() {
        this.data.forEach(this.addItem.bind(this))
    }

    onRootClick(e){
        if (e.target.className === "destroy"){
            this.removeItem(e.target.closest('li'));
            this.root.dispatchEvent(new CustomEvent('removeElement',{
                bubbles: true,
                detail: e.target.closest('li')
            }));

        } else if(e.target.className === "toggle"){
            this.checkUncheckItem(e.target.closest('li'));
        }
    }

    onRootDblClick(e){
        if (e.target.tagName.toLowerCase() === "label"){
            this.updateItem(e.target.closest('li'));
        }
    }

    onRootPressEnter(e){
        if(e.key.toLowerCase() === "enter") {
            if (e.target.tagName.toLowerCase() === "input") {
                this.updateItem(e.target.closest('li'));
            }
        }
    }

}