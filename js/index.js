'use strict';
import ListItem from './list_item.js';
import List from './list.js';
import Footer from "./footer.js"
import EventsForList from "./list_actions.js"

window.addEventListener("load", function(){
    this.inputTodoItems = document.querySelector(".todo");
    this.todoListDiv = document.querySelector(".todo-list-div");
    this.checkAllCheckbox = document.querySelector(".check_all");
    this.section = document.querySelector(".dropdown");
    this.arrItems = [];
    this.listUl = null;
    this.actions = null;
    this.checkItemsLeft = checkItemsLeft;
    this.footer = new Footer();
    inputTodoItems.addEventListener("keydown", addTodoItem.bind(this));
    checkAllCheckbox.addEventListener("click", checkAll.bind(this, checkAllCheckbox.checked));
});
window.addEventListener("hashchange", function(){footer.filtering()});

// function for insert created list file to DOM
function addTodoItem(k) {
    if (k.keyCode === 13) {
        if (inputTodoItems.value.replace(/ /gi, "") !== "") {
            let listItem = new ListItem(inputTodoItems.value);
            listItem.createListItem();
            arrItems.push(listItem);
            footer.changeNumOfItemsText();
            section.style.setProperty("display", "block");
            if (document.location.hash.replace("#/","") !== "completed"){
                if (listUl !== null){
                    listUl.addItem(listItem)
                } else {
                    listUl = new List(todoListDiv, arrItems);
                    actions = new EventsForList(listUl, footer, arrItems);
                    actions.addListenersToList();
                }
            }

            this.footer.changeNumOfItemsText();
            checkAllCheckbox.checked = checkAllCheckbox.checked ? !checkAllCheckbox.checked : checkAllCheckbox.checked;
            inputTodoItems.value = "";
        }
    }
}

// check and uncheck all items
function checkAll() {
    let status;
    if(checkAllCheckbox.checked){
        status = "completed"
    } else {
        status = "active"
    }
    arrItems.forEach(function (obj) {
        if (obj.status !== status) {
            obj.status = status;
            obj.completeCheckbox.checked = checkAllCheckbox.checked;
            obj.listItem.className = status;
        }
    });
    footer.filtering();
    footer.visibleClearAllButton();
    footer.changeNumOfItemsText();
}

// display section footer
function checkItemsLeft() {
    if (arrItems.length > 0){
        section.style.setProperty("display", "block")
    } else {
        section.style.setProperty("display", "none")
    }
}